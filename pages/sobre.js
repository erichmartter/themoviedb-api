
import Link from 'next/link'
import Head from 'next/head'
import Image from 'next/image'
import styles from '../styles/Home.module.css'

const Sobre = ({ author }) => {
  return (
    <div className={styles.container}>
      <Head>
        <title>Create Next App</title>
        <meta name="description" content="Generated by create next app" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <h1 className={styles.title}>
          Sobre: 
        </h1>

        Sistema usando Next.js, para aprender sobre os tipos de renderizações de páginas.

        <hr/>

        Autor: {author}

        <hr/>
        
        <Link href={"/"}>Início</Link>

      </main>
    </div>
  )
}

export default Sobre

export async function getStaticProps() {

  return {
    props: {
      author: 'Erich'
    }
  }
}
